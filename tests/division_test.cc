#include "division_test.h"
#include "arithmetique.h"
#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <limits>

// Enregistrer la classe de test dans le registre de la suite
CPPUNIT_TEST_SUITE_REGISTRATION(divisionTest);

void divisionTest::setUp() {}

void divisionTest::tearDown() {}

void divisionTest::division_normal()
{
  dividende = 6;
  diviseur = 3;
  CPPUNIT_ASSERT_EQUAL(static_cast<long int>(2),
                       static_cast<long int>(arithmetique::division(dividende, diviseur))
    );
  diviseur = -3;
  CPPUNIT_ASSERT_EQUAL(static_cast<long int>(-2),
                       static_cast<long int>(arithmetique::division(dividende, diviseur))
    );
}

void divisionTest::division_max()
{
  dividende = std::numeric_limits<int>::max();
  diviseur = 2;
  CPPUNIT_ASSERT_GREATER(static_cast<long int>(arithmetique::division(dividende, diviseur)),
                         static_cast<long int>(dividende)
    );
}

void divisionTest::division_min()
{
  dividende = std::numeric_limits<int>::lowest();
  diviseur = -2;
  CPPUNIT_ASSERT_LESS(static_cast<long int>(arithmetique::division(dividende, diviseur)),
                      static_cast<long int>(dividende)
    );
}

/*void divisionTest::division_zero()
{
  dividende = 1;
  diviseur = 0;
  if(diviseur == 0){
    CPPUNIT_ASSERT_THROW(std::exception, std::exception::what());
  }
}*/
